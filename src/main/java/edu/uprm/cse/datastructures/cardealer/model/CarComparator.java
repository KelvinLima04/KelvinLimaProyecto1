package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;

public class CarComparator implements Comparator<Car> {
	
	/**
	 * Compare the to specified cars by his brand, model and model option.
	 */
	@Override
	public int compare(Car o1, Car o2) {

		String s1 = o1.getCarBrand() + o1.getCarModel() + o1.getCarModelOption();
		String s2 = o2.getCarBrand() + o2.getCarModel() + o2.getCarModelOption();
		
		return s1.compareTo(s2);
	}
}
