package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarComparator;

public class CircularSortedDoublyLinkedList<E> implements SortedList<E> {
	
	/*
	 * Private class of DNode.
	 */
	private static class DNode<E> {

		private E element;
		private DNode<E> next;
		private DNode<E> prev;

		public DNode() {
			this(null);
		}

		public DNode(E element) {
			this.element = element;
			next = null;
			prev = null;
		}

		public E getElement() {
			return element;
		}
		public void setElement(E element) {
			this.element = element;
		}
		public DNode<E> getNext() {
			return next;
		}
		public void setNext(DNode<E> next) {
			this.next = next;
		}
		public DNode<E> getPrev() {
			return prev;
		}
		public void setPrev(DNode<E> prev) {
			this.prev = prev;
		}
	}
	
	/*
	 * Private class of the the CircularSortedDoublyLinkedListIterator.
	 */
	private class CircularSortedDoublyLinkedListIterator<E> implements Iterator<E>{

		private DNode<E> nextNode;

		public CircularSortedDoublyLinkedListIterator() {
			this.nextNode = (DNode<E>) header.getNext();
		}

		@Override
		public boolean hasNext() {
			return nextNode != header;
		}

		@Override
		public E next() {
			if (this.hasNext()) {
				E result = this.nextNode.getElement();
				this.nextNode = this.nextNode.getNext();
				return result;
			} else {
				throw new NoSuchElementException();
			}
		}

	}
	/*
	 * Instance variables.
	 */
	private DNode<E> header;
	private int currentSize;
	private Comparator<E> comp;
	
	/*
	 * Constructors.
	 */
	public CircularSortedDoublyLinkedList() {
		this.header = new DNode<>();
		this.header.setNext(this.header);
		this.header.setPrev(this.header);
		this.currentSize = 0;
	}

	public CircularSortedDoublyLinkedList(Comparator<E> c) {
		this.header = new DNode<>();
		this.header.setNext(this.header);
		this.header.setPrev(this.header);
		this.currentSize = 0;
		this.comp = c;
	}
	
	/*
	 * Create the instance of the iterator of CircularSortedDoublyLinkedListIterator.
	 */
	@Override
	public Iterator<E> iterator() {
		return new CircularSortedDoublyLinkedListIterator<E>();
	}
	
	/**
	 * Add an object to the CircularSortedDoublyLinkedList in the specified order with
	 * the specified comparator.
	 * @return boolean true if the element is added and false if not.
	 */
	@Override
	public boolean add(E obj) {
		if(obj == null) {
			return false;
		} else {
			DNode<E> newNode = new DNode<>(obj);

			if(this.size() == 0) {					// If size of the list is 0, then add the object after the header.
				this.header.setNext(newNode);
				this.header.setPrev(newNode);
				newNode.setNext(this.header);
				newNode.setPrev(this.header);
				this.currentSize++;
				return true;
			} else if(comp.compare(this.header.getNext().getElement(), newNode.getElement()) > 0){	// Check if the next of the header go after the newNode
				newNode.setNext(this.header.getNext());												// if it is true, add the newNode after the header.
				newNode.setPrev(this.header);
				this.header.getNext().setPrev(newNode);
				this.header.setNext(newNode);
				this.currentSize++;
				return true;
			} else {
				DNode<E> currentNode = this.header.getNext();
				while(currentNode != this.header) {
					if(comp.compare(currentNode.getElement(), newNode.getElement()) > 0) {			// Check if the currentNode go after the newNode
						newNode.setNext(currentNode);												// if it is true, add the newNode before the currentNode.
						newNode.setPrev(currentNode.getPrev());
						currentNode.getPrev().setNext(newNode);
						currentNode.setPrev(newNode);
						this.currentSize++;
						return true;
					}
					currentNode = currentNode.getNext();
				}
															// If the while finished and the newNode is not added then
				newNode.setNext(this.header);				// add the newNode before the header.
				newNode.setPrev(this.header.getPrev());
				this.header.getPrev().setNext(newNode);
				this.header.setPrev(newNode);
				this.currentSize++;
			}
			return true;
		}
	}

	/**
	 * The current size of the list
	 * @return currentSize the current size of the list.
	 */
	@Override
	public int size() {
		return this.currentSize;
	}

	/**
	 * Remove the specified element from the list.
	 * @return boolean true if is removed or false if it is not.
	 */
	@Override
	public boolean remove(E obj) {
		int i = this.firstIndex(obj);	// The index of the obj
		if (i < 0) {					// If index is -1 then the obj is not found
			return false;
		} else {
			this.remove(i);	
			return true;
		}
	}

	/**
	 * Remove the element in the specified index or position.
	 * @return boolean true if it is removed or false if it is not.
	 */
	@Override
	public boolean remove(int index) {
		if(index < 0 || index >= this.size()) {
			throw new IndexOutOfBoundsException("Illegal index.");
		} else {

			DNode<E> temp = this.header;
			int currentPosition = 0;
			DNode<E> target = null;

			while(currentPosition != index) {			// Find the node and position index
				temp = temp.getNext();
				currentPosition++;
			}
			target = temp.getNext();
			temp.setNext(target.getNext());
			target.getNext().setPrev(temp);
			target.setElement(null);
			target.setNext(null);
			target.setPrev(null);
			this.currentSize--;
			return true;	
		}
	}

	/**
	 * Remove all elements of the list of a specific obj.
	 * @return count the total elements removed.
	 */
	@Override
	public int removeAll(E obj) {
		int count = 0;
		while(this.remove(obj)) {
			count++;
		}
		return count;
	}
	
	/**
	 * Find the first element in the list.
	 * @return E the element in the first node after the header.
	 */
	@Override
	public E first() {
		if (this.isEmpty()) {
			return null;
		}
		return this.header.getNext().getElement();
	}

	/**
	 * Find the last element in the list.
	 * @return E the element in the last node before the header.
	 */
	@Override
	public E last() {
		if (this.isEmpty()) {
			return null;
		}
		return this.header.getPrev().getElement();
	}

	/**
	 * Get the element in the specified index.
	 * @return E the element in the node at position index.
	 */
	@Override
	public E get(int index) {
		if(index < 0 || index >= this.size()) {
			throw new IndexOutOfBoundsException("Illegal index.");
		}

		DNode<E> temp = this.getPosition(index);
		return temp.getElement();
	}

	// Helper method to get the DNode at position index
	private DNode<E> getPosition(int index) {
		int currentPosition = 0;
		DNode<E> temp = this.header.getNext();

		while(currentPosition != index) {
			temp = temp.getNext();
			currentPosition++;
		}
		return temp;
	}

	/**
	 * Remove all elements in the list.
	 */
	@Override
	public void clear() {
		while(!this.isEmpty()) {
			remove(0);
		}
	}

	/**
	 * Check if an element is in the list.
	 * @return boolean true if element e is in the list or false if not.
	 */
	@Override
	public boolean contains(E e) {
		return firstIndex(e) >= 0;
	}

	/**
	 * Check if the list is empty.
	 * @return boolean true if the size is 0 or false if is not 0.
	 */
	@Override
	public boolean isEmpty() {
		return this.size() == 0;
	}

	/**
	 * Find the index of the first object that is equal to the specified element.
	 * @return i if its found or -1 if not found.
	 */
	@Override
	public int firstIndex(E e) {
		int i = 0;
		for (DNode<E> temp = this.header.getNext(); temp != this.header; temp = temp.getNext(), i++) {
			if (temp.getElement().equals(e)) {
				return i;
			}
		}
		// Not found
		return -1;
	}

	/**
	 * Find the index of the last object that is equal to the specified element.
	 * @return i if its found or -1 if not found.
	 */
	@Override
	public int lastIndex(E e) {
		int i = this.currentSize - 1;
		for (DNode<E> temp = this.header.getPrev(); temp != this.header; temp = temp.getPrev(), i--) {
			if (temp.getElement().equals(e)) {
				return i;
			}
		}
		// Not found
		return -1;
	}

	/**
	 * Return the list in an array of objects.
	 * @return Object[] the array of object.
	 */
	public Object[] toArray() {
		DNode<E> currentNode = header.getNext();
		Object[] newArray = new Object[this.currentSize];
		for(int i = 0; i < newArray.length; i++) {
			if(currentNode == header) {
				break;
			} else {
				newArray[i] = currentNode.getElement();
				currentNode = currentNode.getNext();
			}
		}
		return newArray;
	}
}
