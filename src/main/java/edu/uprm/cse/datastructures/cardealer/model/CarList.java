package edu.uprm.cse.datastructures.cardealer.model;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarComparator;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;
import edu.uprm.cse.datastructures.cardealer.util.SortedList;

public class CarList {
	
	private static CarList singleton = new CarList();
	private static final SortedList<Car> carList = (SortedList<Car>) new CircularSortedDoublyLinkedList<Car>(new CarComparator());
	
	private CarList() {
	}
	
	public static SortedList<Car> getInstance() {
		return carList;
	}
	
	public static void resetCars() {
		carList.clear();;
	}
}
