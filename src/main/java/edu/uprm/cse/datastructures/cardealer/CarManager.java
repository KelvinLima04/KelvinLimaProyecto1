package edu.uprm.cse.datastructures.cardealer;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarList;
import edu.uprm.cse.datastructures.cardealer.util.SortedList;


@Path("/cars")
public class CarManager {

	private final SortedList<Car> cList = CarList.getInstance();
	
	
	/**
	 * Get all the cars in the array.
	 * @return cars the array of cars.
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getAllCars() {
		if(this.cList.isEmpty()) return null;
		Car[] cars = new Car[this.cList.size()];
		for(int i = 0; i < this.cList.size(); i++) { // If not empty add the cars to the array
			cars[i] = (Car) this.cList.get(i);
		}
		return cars;
	}
	
	/**
	 * Get the car with the same id.
	 * @param id the id of the wanted car.
	 * @return Car the car with the same id.
	 */
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car getCar(@PathParam("id") long id) {
		for(int i = 0; i < this.cList.size(); i++) {
			if(this.cList.get(i).getCarId() == id) { // If the id is equals to the car in the position i
				return this.cList.get(i);			 // then return the car.
			}
		}
		throw new NotFoundException(new JsonError("Error", "Car " + id + " not found"));
	}
	
	/**
	 * Add a car to the list, if this id car exist then it is not added.
	 * @param c the car to add to the list.
	 * @return Response the response with the specified status if it is added or not.
	 */
	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addCar(Car c) {

		if(c.getCarBrand() == null || c.getCarId() < 0 || c.getCarModel() == null 
				|| c.getCarModelOption() == null || c.getCarPrice() == 0) {			// Check if the car c is valid.
			return Response.status(Response.Status.NOT_ACCEPTABLE).build();
		}
		for(int i = 0; i < this.cList.size(); i++) {
			if(this.cList.get(i).getCarId() == c.getCarId()) {				// If the car in the position i is equal to the
				return Response.status(Response.Status.CONFLICT).build();	// car c then it cannot be added.
			}
		}
		this.cList.add(c);													// If not then add the car c.
		return Response.status(201).build();
	}
	
	/**
	 * Update a car in the list with the specified id.
	 * @param id the id of the car you want to update.
	 * @param c the updated car.
	 * @return Response the response with the specified status if it is added or is not found.
	 */
	@PUT
	@Path("/{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCar(@PathParam("id") long id, Car c) { 
		for(int i = 0; i < this.cList.size(); i++) {
			if(this.cList.get(i).getCarId() == id) {				// If the id of the car in the position i is
				this.cList.remove(cList.get(i));					// equal to the id of the car c, then remove it
				this.cList.add(c);									// and add the updated car c.
				return Response.status(Response.Status.OK).build();
			}
		}
		return Response.status(Response.Status.NOT_FOUND).build();
	}
	
	/**
	 * Delete a car in the list with the specified id.
	 * @param id the id of the car to delete.
	 * @return Response the response with the specified status if it is deleted or not found.
	 */
	@DELETE
	@Path("/{id}/delete")
	public Response deleteCar(@PathParam("id") long id) {
		try {
			cList.remove(getCar(id));									// Try removing the car with the specified id.
		} catch (NotFoundException e) {									
			return Response.status(Response.Status.NOT_FOUND).build();	// not found
		}
		return Response.status(Response.Status.OK).build();				// found
	}
}
